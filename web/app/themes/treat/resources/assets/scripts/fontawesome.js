// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
// import the Facebook and insta icons
import {faFacebook, faInstagram, faTwitter} from '@fortawesome/free-brands-svg-icons';

// add the imported icons to the library
library.add(faFacebook, faInstagram, faTwitter);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();
