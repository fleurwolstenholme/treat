{{--
  Template Name: Contact Template
--}}

@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp

    <div class="container">
      <div class="row py-5">
        <div class="col-lg-6 col-sm-12  p-5 text-right text-info">
            <h3 class="py-5 text-primary"> get in touch </h3>
          {!! the_content() !!}
          
          <div class="swpm-payment-button"><div class="swpm-button-wrapper swpm-stripe-buy-now-wrapper"><form id="swpm-stripe-payment-form-f57067f7c5e2e43ed9b9d05ce6c45400" action="https://www.treat.nz/?swpm_process_stripe_sca_subscription=1&amp;ref_id=swpm_f57067f7c5e2e43ed9b9d05ce6c45400|348" method="POST"> <div style="display: none !important"><script src="https://js.stripe.com/v3/"></script><link rel="stylesheet" href="https://checkout.stripe.com/v3/checkout/button.css" type="text/css" media="all">	<script>
		var stripe = Stripe('pk_live_51H9krLLlY3XWGWDjShftytjeVxf8dnCcxzVastn0zZVzG7Z0B8iJPG7iX2MOb1xHC3j0ej3YxQ2mzlPTVef8UkFQ00ofwwENkO');
		jQuery('#swpm-stripe-payment-form-f57067f7c5e2e43ed9b9d05ce6c45400').on('submit',function(e) {
			e.preventDefault();
			var btn = jQuery(this).find('button').attr('disabled', true);
			jQuery.post('https://www.treat.nz/wp-admin/admin-ajax.php', {
				'action': 'swpm_stripe_sca_create_checkout_session',
				'swpm_button_id': 348,
				'swpm_page_url': 'https://www.treat.nz/contact/',
				'swpm_uniqid': 'f57067f7c5e2e43ed9b9d05ce6c45400'
				}).done(function (response) {
					if (!response.error) {
						stripe.redirectToCheckout({sessionId: response.session_id}).then(function (result) {
					});			
					} else {
						alert(response.error);
						btn.attr('disabled', false);
						return false;
					}
			}).fail(function(e) {
				alert("HTTP error occurred during AJAX request. Error code: "+e.status);
				btn.attr('disabled', false);
				return false;
			});
		});
	</script>
	</div><button id="348" type="submit" class="btn btn-primary"><span>Subscribe</span></button></form></div></div>
	
	
        </div>
        
         <div class="form col-lg-6 col-sm-12 border-secondary text-center shadow-lg p-3 mb-5 bg-white">
          {!! do_shortcode('[contact-form-7 id="5" title="Contact form 1"]') !!}
        </div>
        
      </div>
    </div>
@endwhile
@endsection
