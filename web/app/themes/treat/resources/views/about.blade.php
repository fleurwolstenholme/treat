{{--
  Template Name: About Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  <div class="container">
      <div class="row">

        <div class="col-lg-12 col-sm-12 text-center align-self-center">
            <div class="bg-success p-5 text-white bubble">
                <h1>Our Story</h1>
                <p>{!! the_content() !!}</p>
                
                <div class="swpm-payment-button"><div class="swpm-button-wrapper swpm-stripe-buy-now-wrapper"><form id="swpm-stripe-payment-form-8593be684c9a9894329e684cc8a81e58" action="https://www.treat.nz/?swpm_process_stripe_sca_subscription=1&amp;ref_id=swpm_8593be684c9a9894329e684cc8a81e58|456" method="POST"> <div style="display: none !important"><script src="https://js.stripe.com/v3/"></script><link rel="stylesheet" href="https://checkout.stripe.com/v3/checkout/button.css" type="text/css" media="all">	<script>
		var stripe = Stripe('pk_live_51H9krLLlY3XWGWDjShftytjeVxf8dnCcxzVastn0zZVzG7Z0B8iJPG7iX2MOb1xHC3j0ej3YxQ2mzlPTVef8UkFQ00ofwwENkO');
		jQuery('#swpm-stripe-payment-form-8593be684c9a9894329e684cc8a81e58').on('submit',function(e) {
			e.preventDefault();
			var btn = jQuery(this).find('button').attr('disabled', true);
			jQuery.post('https://www.treat.nz/wp-admin/admin-ajax.php', {
				'action': 'swpm_stripe_sca_create_checkout_session',
				'swpm_button_id': 456,
				'swpm_page_url': 'https://www.treat.nz/about/',
				'swpm_uniqid': '8593be684c9a9894329e684cc8a81e58'
				}).done(function (response) {
					if (!response.error) {
						stripe.redirectToCheckout({sessionId: response.session_id}).then(function (result) {
					});			
					} else {
						alert(response.error);
						btn.attr('disabled', false);
						return false;
					}
			}).fail(function(e) {
				alert("HTTP error occurred during AJAX request. Error code: "+e.status);
				btn.attr('disabled', false);
				return false;
			});
		});
	</script>
	</div><button id="456" type="submit" class="btn btn-primary"><span>Subscribe</span></button></form></div></div>
        
            </div>
        </div>
        </div>
        
        <div class="row">
        <div class="col-lg-6 col-sm-12 text-center align-self-center">
        <div class="deal bg-primary bubble text-white">
            <h4 class="px-3 pt-3">How To Use</h4>
        <p class="p-3">Hit subscribe & get your digital pass. Search treats near you on our website, book, show your digital pass when paying, save & enjoy!</p>
       </div>
        <div class="deal bg-warning bubble text-white mt-2">
          <h4 class="p-3">241 on mains | 40% off food </h4>
        </div>
        <div class="deal bg-success bubble text-white mb-5">
        <img class="icon p-1" src=" @asset('images/ic_card_giftcard_24px.png.') " aria-hidden="true"> <h5>Deal of the Day</h5><p>Here one day, gone the next!</p>
        </div>
        </div>

        <div class="col-lg-6 col-sm-12 text-center align-self-center">
            <div class="bubble bg-primary text-white p-3"> 
              <h1 class="pb-4">Support Local</h1>
              <p class="pb-3">{!! $support = get_field('support') !!}</p>
            </div>
            </div>
        </div>
  </div>
  @endwhile
@endsection
