{{--
  Template Name: FAQ Template
--}}

@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp

  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-12 text-info">
        @include('partials.page-header')
      </div>
      <div class="col-lg-8 col-sm-12 p-4 d-flex justify-content-end text-right">
        <p class="text-primary">Treat members enjoy <strong> 2 for 1 on mains </strong> or <strong> 40% off food </strong> at participating restaurants.</p>
      </div>
      <div class="col-12 d-flex justify-content-end text-right">
        <a class="text-info" href="https://www.treat.nz/about/"><i>learn more</i></a>
      </div>
    </div>
  </div>


      <div class="container p-5">
        @foreach($faq['faq'] as $faq)

          <div class="row">
            <div class="col-lg-4 col-sm-12 p-5 text-info">
              {{ $faq['question'] }}
            </div>
            <div class="col-lg-8 col-sm-12 p-5 text-info">
              {{ $faq['answer'] }}
            </div>
          </div>
          <hr>
        @endforeach
      </div>


@endwhile
@endsection
