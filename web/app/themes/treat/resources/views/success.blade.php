{{--
  Template Name: Success Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
 
<div class="container">
        <div class="text-center align-self-center">
            <div class="bg-warning p-5 text-white bubble">
                <h1>@include('partials.page-header')</h1>
                <p>{!! the_content() !!}</p>
            </div>
        </div>
</div>
  @endwhile
@endsection
