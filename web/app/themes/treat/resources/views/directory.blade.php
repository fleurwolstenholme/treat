{{--
  Template Name: Directory Template
--}}

@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp
  <section id="directory" class="page-section bg-light">
    <h1 class="text-info text-center py-4">{!! App::title() !!}</h1>
    <h6 class="text-info text-center py-2">Here one day, gone the next... Every 24 hours a new deal will appear for each town! </h6>
    <div class="container">
      <div class="row">
        @foreach($restaurant['restaurant'] as $restaurant)
          <div class="col-sm-12 col-md-6 mx-auto col-lg-4">
            <div class="card mb-4">
              <img class="card-img-top lazy" src="{{ $restaurant['image']['sizes']['medium'] }}" alt="{{ $restauramt['image']['alt'] }}">
              <div class="card-body">
                <h5 class="card-title text-info">{{ $restaurant['name'] }}</h5>
                <p class="text-primary float-right">{{ $restaurant['days_available'] }}</p>
                <p class="text-info">{{ $restaurant['deal'] }} </p>
                <p class="card-text text-center text-info">{{ $restaurant['description'] }}</p>
            </div>
  
            <div class="card-footer">
              <div class="row">
                <div class="col-3">
                <a class="btn btn-secondary p-3" target="blank"
                  href="{{ $restaurant['book'] }}">book</strong></a></div>
    
                <div class="col-9 align-self-end text-success">
                  <ul class="list-unstyled text-right">
                    <li> {{ $restaurant['number'] }}
                      {{ $restaurant['street_name'] }} </li>
                    <li> {{ $restaurant['town'] }} </li>
                    <li> {{ $restaurant['postcode'] }} </li>
                    <li> {{ $restaurant['contact_number'] }} </li>
                  </ul>
                </div>
              </div>
            </div>
  
          </div>
        </div>
        @endforeach
  
      </div>
    </div>
  </section>
  
@endwhile
@endsection