{{--
  Template Name: User Template
--}}

@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Subscriber List</h2>
  <p class="text-info">{!! the_content() !!}</p>
<br>
  <p>Type something in the input field to search the table for emails:</p>  
  <input class="form-control" id="myInput" type="text" placeholder="Search..">
  <br>
  
  
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Email</th>
      </tr>
    </thead>
    <tbody id="myTable">
<?php /* Template Name: Get User Emails */ 

$all_users = get_users();
foreach ($all_users as $user) {
    echo '<tr>';
	echo '<td>';
	echo($user->user_email);
	echo '</td>';
}
echo '</tr>'; 
?>
    </tbody>
  </table>
</div>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

</body>
</html>


@endwhile
@endsection