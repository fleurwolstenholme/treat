<div class="container mt-3 align-self-start w-100">
<div id="bannerCarousel" class="carousel slide carousel-fade" data-interval="5000" data-ride="carousel">
    <div class="carousel-inner">
    @foreach($carousel as $item)
      <div class="carousel-item {{ ($loop->first) ? 'active' : '' }}">
        <div class="embed-responsive embed-responsive-16by9">
          {!! wp_get_attachment_image($item['ID'], 'full', false, ['class' => 'embed-responsive-item object-fit-cover object-position-top']) !!}
        </div>
      </div>
    @endforeach
    </div>
    <!--Indicators-->
    <div class="carousel-indicators py-4 position-relative">
      @foreach($carousel as $item)
        <div data-target="#bannerCarousel" data-slide-to="{{ $loop->index }}" {{ ($loop->first) ? 'class=active' : '' }}></div>
      @endforeach
    </div>
  </div>
  </div>