<article @php post_class('content-card') @endphp>
    
<?php

// vars
$image = get_field('image');

?>

<article @php post_class('content-card') @endphp>
  <header>
      <?php if( $image ): ?>
		<img class="img-thumbnail object-fit-cover" src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" />
	<?php endif; ?>
	    <h4 class="text-center"><?php the_field('name'); ?></h4>
  </header>
  
  <div class="entry-text text-center text-info">
   <p><?php the_field('deal'); ?> | 
   <?php the_field('days_available'); ?></p>
  </div>
  <a class="align-items-center bubble p-2 d-flex justify-content-center bg-primary text-center text-white" href="{{ get_permalink() }}">View Details</a>
</article>