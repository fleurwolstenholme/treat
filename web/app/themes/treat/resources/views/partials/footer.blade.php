<footer class="page-footer text-info bg-white">

  <div class="container px-4">
    <hr>
    <div class="row pb-4 d-flex justify-content-center">

      <div class="col-lg-2">
        <p class="mt-2 text-center">
          <img src=" @asset('images/treatg.png.') " alt="treat.nz">
      </div>

      <div class="col-sm-12 col-lg-3 p-4 d-none d-md-block">
        <ul class="list-unstyled">
          <li><a href="/">Home</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/restaurant-opportunities">Become a Treater</a></li>
          <li><a href="/category/all/">Where to eat</a></li>
          <li><a href="/contact">Contact</a></li>
        </ul>
      </div>

      <div class="col-sm-12 col-lg-3 py-4">
        <ul class="list-unstyled">
          <li><a href="/contact/faqs">FAQs</a></li>
          <li><a href="/contact/privacy-policy">Privacy Policy</a></li>
          <li><a href="/contact/tcs">T&Cs</a></li>
        </ul>
      </div>

      <div class="col-sm-12 col-lg-2 py-4 follow">

<div class="swpm-payment-button"><div class="swpm-button-wrapper swpm-stripe-buy-now-wrapper"><form id="swpm-stripe-payment-form-157e3b588e7e89f984061e5cb22b7a33" action="https://www.treat.nz/?swpm_process_stripe_sca_subscription=1&amp;ref_id=swpm_157e3b588e7e89f984061e5cb22b7a33|456" method="POST"> <div style="display: none !important"><script src="https://js.stripe.com/v3/"></script><link rel="stylesheet" href="https://checkout.stripe.com/v3/checkout/button.css" type="text/css" media="all">	<script>
		var stripe = Stripe('pk_live_51H9krLLlY3XWGWDjShftytjeVxf8dnCcxzVastn0zZVzG7Z0B8iJPG7iX2MOb1xHC3j0ej3YxQ2mzlPTVef8UkFQ00ofwwENkO');
		jQuery('#swpm-stripe-payment-form-157e3b588e7e89f984061e5cb22b7a33').on('submit',function(e) {
			e.preventDefault();
			var btn = jQuery(this).find('button').attr('disabled', true);
			jQuery.post('https://www.treat.nz/wp-admin/admin-ajax.php', {
				'action': 'swpm_stripe_sca_create_checkout_session',
				'swpm_button_id': 456,
				'swpm_page_url': 'https://www.treat.nz/contact/',
				'swpm_uniqid': '157e3b588e7e89f984061e5cb22b7a33'
				}).done(function (response) {
					if (!response.error) {
						stripe.redirectToCheckout({sessionId: response.session_id}).then(function (result) {
					});			
					} else {
						alert(response.error);
						btn.attr('disabled', false);
						return false;
					}
			}).fail(function(e) {
				alert("HTTP error occurred during AJAX request. Error code: "+e.status);
				btn.attr('disabled', false);
				return false;
			});
		});
	</script>
	</div><button id="456" type="submit" class="btn btn-primary"><span>Subscribe</span></button></form></div></div>
	
	
          <a target="_blank" rel="noopener" href="{{ $social_media['facebook'] }}"><i
              class="site-footer__icons fab fa-facebook"></i></a>
          <a target="_blank" rel="noopener" href="{{ $social_media['twitter'] }}"><i
              class="site-footer__icons fab fa-twitter"></i></a>
          <a target="_blank" rel="noopener" href="{{ $social_media['instagram'] }}"><i
              class="site-footer__icons fab fa-instagram"></i></a>
      </div>
    </div>

      <div class="row">
        <div class="col-s-12 col-lg-12 p-2 text-center text-primary">
          <p><u><a href="https://www.treat.nz"></a></u> © 2020 treat.nz</p>
          <p class="h6">© All right Reversed.<a class="text-green ml-2" href="https://www.treat.nz" target="_blank">Treat.NZ</a></p>
        </div>
        <hr>
      </div>

    </div>


<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/189cd7fcd321c154b354dae02/29413fee43e42a447c5fb994e.js");</script>

</footer>