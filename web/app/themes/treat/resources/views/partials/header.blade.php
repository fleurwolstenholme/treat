<header>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-174949887-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-174949887-1');
</script>



  <nav class="nav-primary navbar navbar-expand-lg navbar-light bg-light">

            <?php
  $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
  echo "<a href='$url'>back</a>"; 
?>

  <a class="navbar-brand align-self-center px-5 mx-5" href="{{ home_url('/') }}"> <img src=" @asset('images/treatg.png.') " alt="treat.nz"></a>

  <button id="navBtn" class="navbar-toggler hamburger hamburger--vortex" type="button" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>



  <div class="collapse navbar-collapse pt-4 mt-1" id="primaryNav">

    <ul class="navbar-nav ml-auto px-5">
        
                <div class="swpm-payment-button"><div class="swpm-button-wrapper swpm-stripe-buy-now-wrapper"><form id="swpm-stripe-payment-form-fe0ca9fcf48936cf376566f862bf9dad" action="https://www.treat.nz/?swpm_process_stripe_sca_subscription=1&amp;ref_id=swpm_fe0ca9fcf48936cf376566f862bf9dad|456" method="POST"> <div style="display: none !important"><script src="https://js.stripe.com/v3/"></script><link rel="stylesheet" href="https://checkout.stripe.com/v3/checkout/button.css" type="text/css" media="all">	<script>
		var stripe = Stripe('pk_live_51H9krLLlY3XWGWDjShftytjeVxf8dnCcxzVastn0zZVzG7Z0B8iJPG7iX2MOb1xHC3j0ej3YxQ2mzlPTVef8UkFQ00ofwwENkO');
		jQuery('#swpm-stripe-payment-form-fe0ca9fcf48936cf376566f862bf9dad').on('submit',function(e) {
			e.preventDefault();
			var btn = jQuery(this).find('button').attr('disabled', true);
			jQuery.post('https://www.treat.nz/wp-admin/admin-ajax.php', {
				'action': 'swpm_stripe_sca_create_checkout_session',
				'swpm_button_id': 456,
				'swpm_page_url': 'https://www.treat.nz/about/',
				'swpm_uniqid': 'fe0ca9fcf48936cf376566f862bf9dad'
				}).done(function (response) {
					if (!response.error) {
						stripe.redirectToCheckout({sessionId: response.session_id}).then(function (result) {
					});			
					} else {
						alert(response.error);
						btn.attr('disabled', false);
						return false;
					}
			}).fail(function(e) {
				alert("HTTP error occurred during AJAX request. Error code: "+e.status);
				btn.attr('disabled', false);
				return false;
			});
		});
	</script>
	</div><button id="456" type="submit" class="btn btn-primary"><span>Subscribe</span></button></form></div></div>
	
      @foreach ($primary_navigation as $item)
      <li class="nav-item {{ $item->classes ?? '' }} {{ $item->active ? 'active' : '' }}">
        <a id="navLink{{ $item->id }}" class="nav-link" href="{{ $item->url }}">{{ $item->label }}</a>
      </li>
    @endforeach
  </ul>
  </div>
  

</nav>
</header>

