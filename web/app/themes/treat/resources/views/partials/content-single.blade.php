<section class="page-section">
  <div class="container">
       <?php
      $image = get_field('image');
      ?>
    <article @php post_class() @endphp>
      <header class="text-center align-self-center">
       <?php if( $image ): ?>
			<img class="img-thumbnail h-75 w-75" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif; ?>
        <h1 class="text-center p-3 text-info entry-title">{!! get_the_title() !!} 
                                <a class="btn btn-secondary p-2" target="blank"
                 href="<?php the_field('book'); ?>">book</strong></a></h1>
      </header>
     

      <div class="row p-3">
          <div class="col-lg-6 col-sm-6">
                <h4 class="text-center text-info"><?php the_field('deal'); ?></h4> </div>
        <div class="col-lg-6 col-sm-6">
                <h4 class="text-center text-primary">Available <?php the_field('days_available'); ?></h4>
            </div>
     </div>
  
        <div class="row pb-5">
            <p class="col-lg-12 col-sm-12 text-center text-info"><?php the_field('description'); ?></p>
                <div class="col-lg-12 col-sm-12 text-center text-success">
                  <p><?php the_field('address'); ?></p>
                </div>
              </div>
  
          </div>
    </article>
</section>
