{{--
  Template Name: restaurantopportunity Template
--}}

@extends('layouts.app')

@section('content')
@while(have_posts()) @php the_post() @endphp
<div class="container mt-3">
    <div class="container text-center bubble bg-primary mx-auto p-5">
      <div class="row text-center">
          <div class="col-12 blurb text-white">
        <h1 class="p-3">{!! App::title() !!}</h1>
        <hr class="p-3">
        <p> {!! the_content() !!}</p>
          </div>
      </div>

      <div class="row">
        @foreach($discounts['discounts'] as $discounts)
          <div class="col-lg-3 col-sm-8 bubble mx-auto align-self-center text-info bg-white p-4 m-2">
            {{ $discounts['title'] }}
          </div>
        @endforeach
      </div>
      </div>



<div class="row d-flex justify-content-center my-5">
    <div class="col-lg-5 col-md-8 bubble m-4 p-4 bg-info text-center text-white">
        <h2>Benefits</h2>
        <h6><strong>Advertising</strong></h6>
        <p class="text-white">Through digital marketing platforms and social channels, enjoy free marketing to a database of highly engaged, active members looking for their next meal.</p>
        <h6><strong>New Customers & More Bookings</strong></h6>
        <p class="text-white">With treat.nz being customisable to suit your business, benefit from both more bookings and new customers trying your restaurant thanks to treat.nz.</p>
        <h6><strong>It's Free!</strong></h6>
        <p class="text-white">Joining treat.nz is free for restaurants who offer either the 2 for 1 deal on mains or 40% off the food bill. If it works, you benefit from new and more frequent bookings as well as extensive marketing. If it doesn’t it doesn’t cost you anything!</p>
    </div>

  <div class="text-center col-lg-5 col-md-8 bubble m-4 p-4 bg-primary">
    <h2 class="text-white">Deal of The Day</h2>
    <p class="text-white">If our free options do not suit your business. Checkout Deal of the Day. <br> Advertise to our active members as the Deal of the Day for your area. <br> Published for 24hrs consecutively throughout website and social media platforms. <br> Advertisements are paid. Fees are significantly lower than print media or social media campaigns! Each restaurant may advertise up to 4 days a month, this can be a mix of deals or repeats.</h6>
</div>

<div class="text-right col-lg-10 text-center col-sm-8 bubble m-4 p-4 bg-warning">
    <h2 class="text-white">We'd love to hear from you!</h2>
    <p class="text-white">Join our free marketing platform for locally owned restaurants! Get more information, express interest, just say hi...We'll respond within 3 working days.</p>
    <a href="mailto:restaurant@treat.nz" target="_blank" class="btn">hello@treat.nz</a>
</div>
</div>

</div>




@endwhile
@endsection
