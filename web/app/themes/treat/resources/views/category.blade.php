<section id="restaurants">
@extends('layouts.app')

@section('content')

<div class="container">
  <h2 class="text-center p-4 text-primary"> Find Treats! </h2>

<div class="row align-items-center text-center">
          @foreach(get_terms(['taxonomy'=>'category']) as $cat)
              <a class="col text-center align-self-center" href="{{ get_term_link($cat) }}">
               <p class="text-center h4 align-self-center p-1 text-info">{{ $cat->name }}</p> 
              </a>
          @endforeach
      </div>




@if(!have_posts())
  <div class="alert alert-warning">
    {{ __('Sorry, no results were found.', 'sage') }}
  </div>
  {!! get_search_form(false) !!}
@endif

@if(have_posts())
  <div class="row">
    @while(have_posts()) @php the_post() @endphp
      <div class="col-lg-4 p-3">
        @include('partials.content-card-'.get_post_type())
        <hr>
      </div>
    @endwhile
  </div>
@endif
</div>

</section>
@endsection
