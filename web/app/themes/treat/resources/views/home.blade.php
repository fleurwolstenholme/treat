{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  @include('partials.banner')

  <section class="container">
      <div class="row p-3">
        <div class="align-self-center col-lg-6 col-sm-12 mx-auto text-left text-info p-4">
          <h2 class="p-2"><strong>How to Treat     <img class="icon" src=" @asset('images/greengiftcard.png.') " aria-hidden="true"></h2></strong>
          <ul class="list-unstyled p-1 h4">
              <li>Subscribe & find treats!</li><br>
              <li class="text-warning">Book a table.</li> <br>
              <li class="text-primary">Present your subscription card on payment.</li> <br>
              <li class="text-warning">Enjoy 241 mains or 40% off food with treat.nz.</li><br>
              <li class="text-success">Join by the 1st October and enjoy 6 months <strong>FREE!</strong></li></ul>
        </div>
        
         <div class="col-lg-5 col-sm-12  text-center p-2 embed-responsive embed-responsive-1by1">
          {!! get_the_post_thumbnail(null, 'full', ['class' => 'embed-responsive-item object-cover']) !!}
          </div>
    </div>
  </section>

  <section class="container">
    <div class="row p-3">
      <div id="bannerCarousel" class="col-lg-6 col-sm-12 mx-auto carousel slide p-2 carousel-fade" data-interval="3000" data-ride="carousel">
        <div class="carousel-inner bubble">
    @foreach($new as $item)
      <div class="carousel-item {{ ($loop->first) ? 'active' : '' }}">
        <div class="embed-responsive embed-responsive-1by1">
          {!! wp_get_attachment_image($item['ID'], 'full', false, ['class' => 'object-position-top embed-responsive-item object-fit-cover']) !!}
        </div>
      </div>
    @endforeach
        </div>
      </div>
      
            <div class="align-self-center col-lg-5 col-sm-12 bg-light bubble text-center text-info p-2">
        <h5>Become a Treater</h5>
        <p>View Restaurant Opportunities</p>
        <hr>
        <p class="bg-primary text-white py-2">Extensive Digital Marketing & Advertising</p>
        <p class="bg-success text-white py-2">New Customers & More Bookings</p>
        <p class="bg-warning text-white py-2">It's FREE!</p>
        <a class="bg-info text-white p-2" href="/restaurant-opportunities">Find Out More</a>
      </div>
    </div>
  </section>


  @endwhile
@endsection
