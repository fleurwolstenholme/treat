<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Log1x\Navi\Navi;

class App extends Controller
{
    
        public function socialMedia()
    {
        return[
            'facebook'=> get_field('facebook', 'option'),
            'instagram'=> get_field('instagram', 'option'),
            'twitter'=> get_field('twitter', 'option'),
        ];
    }

    public function carousel()
    {
        return get_field('carousel', get_option('page_on_front'));
    }


    public function new()
    {
        return get_field('new', get_option('page_on_front'));
    }

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public function primaryNavigation()
    {
        $navigation = (new Navi())->build('primary_navigation');

        if ($navigation->isEmpty()) {
            return;
        }

        return $navigation->toArray();
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function featuredImage() {
        return get_field('featured_image');
    }

    public function faq() {
        return [
            'faq'   => get_field('faq'),
        ];
    }
    
    public function restaurants() {
        return [
            'name'              => get_field('name'),
            'deal'              => get_field('deal'),
            'days_available'    => get_field('days_available'),
            'description'       => get_field('description'),
            'image'             => get_field('image'),
            'address'           => get_field('address'),
            'book'              => get_field('book'),
        ];
    }

    public function discounts() {
        return [
            'discounts'   => get_field('discounts'),
        ];
    }

    public function support() {
        return get_field('support');
    }

    public function restaurant()
    {
        return[
            'restaurant'=> get_field('restaurant'),
            'restaurant_napier'=> get_field('restaurant_napier', 'restaurant'),
            'restaurant_rotorua'=> get_field('restaurant_rotorua', 'restaurant'),
        ];
    }

    public function address() {
        return [
            'address' => get_field('address'),
        ];
    }

    public function pages() {
        $pages = get_pages();
    }
}
